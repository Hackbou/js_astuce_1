let userData = [
  {
    id: 1,
    username: "Abou sow",
    email: "abou@gmail.com",
    password: "passer123",
  },
  {
    id: 2,
    username: "Cheickh Dioum",
    email: "cheickh@gmail.com",
    password: "12345",
  },
  {
    id: 3,
    username: "Abdoulay Ba",
    email: "abdoulaye@gmail.com",
    password: "jepasse123",
  },
];

//Fonction qui va afficher les donnees
function read(data = []) {
  console.log(data);
}

// Function qui permet de créer un compte utilisateur dans la base de données
function create(newItem) {
  userData.push({ ...newItem, id: userData.length + 1 });
}

// Fonction qui nous permet de modifier un utilisateur
function updateabdoulaye(id) {
  let userFinded = userData.find((item) => item.id === id);

  if (userFinded) {
    userFinded.password = "abou123";

    const restUser = userData.filter((user) => user.id !== id);

    userData = [...restUser, userFinded];
  } else {
    alert("Cet utilisateur n'existe pas dans la base de donnee");
  }
}

// Fonctionqui nou permet de supprimer un utilisateur
function removeabdoulaye(id) {
  // On recupere l'utilisateur a partir d'un ID

  let userFinded = userData.find((item) => item.id === id);
  if (userFinded) {
    userData = userData.filter((user) => user.id !== id);
  } else {
    alert("Cet utilisateur n'existe pas dans la base de donnee");
  }
}

create({
  username: "alallala",
  email: "lalal@gmail.com",
  password: "lalal123",
});

create({
  username: "apapapa",
  email: "papapa@gmail.com",
  password: "lpapapal123",
});

create({
  username: "quququququ",
  email: "quququq@gmail.com",
  password: "lpapapal123",
});

updateabdoulaye(6);
updateabdoulaye(5);
removeabdoulaye(6);
removeabdoulaye(10);

read(userData);
