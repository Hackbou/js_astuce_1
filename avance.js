import { CRUD } from "./crud.js";

let userData = [
  {
    id: 1,
    username: "Abou sow",
    email: "abou@gmail.com",
    password: "passer123",
  },
  {
    id: 2,
    username: "Cheickh Dioum",
    email: "cheickh@gmail.com",
    password: "12345",
  },
  {
    id: 3,
    username: "Abdoulay Ba",
    email: "abdoulaye@gmail.com",
    password: "jepasse123",
  },
];

CRUD.create({
  username: "Coumba",
  email: "coumba@gmail.com",
  password: "lalal123",
});

CRUD.create({
  username: "Sadio",
  email: "sadio@gmail.com",
  password: "passer2023",
});

// CRUD.delete(5);

CRUD.update(1, { attribute: "email", value: "cbba@gmail.sn" });

CRUD.removePassword();
CRUD.read();
