let userData = [
  {
    id: 1,
    username: "Abou sow",
    email: "abou@gmail.com",
    password: "passer123",
  },
  {
    id: 2,
    username: "Cheickh Dioum",
    email: "cheickh@gmail.com",
    password: "12345",
  },
  {
    id: 3,
    username: "Abdoulay Ba",
    email: "abdoulaye@gmail.com",
    password: "jepasse123",
  },
];

//Fonction qui va afficher les donnees
function read() {
  console.log(userData);
}

// Function qui permet de créer un compte utilisateur dans la base de données
function create() {
  const newUser = {
    id: 4,
    username: "New user",
    email: "newuser@gmail.com",
    password: "passer123",
  };

  userData.push(newUser);
}

// Fonction qui nous permet de modifier un utilisateur
function update() {
  let userFinded = userData.find((item) => item.id === 2);
  if (userFinded) {
    userFinded.password = "abou123";

    const restUser = userData.filter((user) => user.id !== 2);

    userData = [...restUser, userFinded];
  } else {
    alert("Cet utilisateur n'existe pas dans la base de donnee");
  }
}

// Fonctionqui nou permet de supprimer un utilisateur
function remove() {
  // On recupere l'utilisateur a partir d'un ID
  const id = 1;
  let userFinded = userData.find((item) => item.id === id);
  if (userFinded) {
    userData = userData.filter((user) => user.id !== id);
  } else {
    alert("Cet utilisateur n'existe pas dans la base de donnee");
  }
}

create();
update();
remove();

read();
