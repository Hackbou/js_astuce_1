export const CRUD = {
  data: [],

  read: function read() {
    console.log(this.data);
  },

  create: function create(newItem) {
    this.data.push({ ...newItem, id: this.data.length + 1 });
  },

  update: function updateabdoulaye(id, { attribute, value }) {
    let userFinded = this.data.find((item) => item.id === id);

    if (userFinded) {
      userFinded[attribute] = value;

      const restUser = this.data.filter((user) => user.id !== id);

      this.data = [...restUser, userFinded];
    } else {
      alert("Cet utilisateur n'existe pas dans la base de donnee Update");
    }
  },

  delete: function removeabdoulaye(id) {
    let userFinded = this.data.find((item) => item.id === id);
    if (userFinded) {
      this.data = this.data.filter((user) => user.id !== id);
    } else {
      alert("Cet utilisateur n'existe pas dans la base de donnee delete");
    }
  },

  removePassword: function removeAttribe() {
    const data = this.data.map((item) => {
      const { password, ...resItem } = item;
      return resItem;
    });

    this.data = data;
  },
};
